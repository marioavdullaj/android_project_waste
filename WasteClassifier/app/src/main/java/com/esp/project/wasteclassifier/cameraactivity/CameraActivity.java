package com.esp.project.wasteclassifier.cameraactivity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.esp.project.wasteclassifier.R;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * This class provides the implementation of the camera activity and its functionalities.
 */
public class CameraActivity extends AppCompatActivity {

    // The request code used in ActivityCompat.requestPermissions()
    // and returned in the Activity's onRequestPermissionsResult()
    private static final int REQUEST_SAVING = 3;
    private static final int REQUEST_CLASSIFY = 2;
    private static final int PERMISSION_ALL = 1;
    //Log TAG for this particular activity
    private static final String CAM_TAG = "Camera ---> ";

    /**
     * The constant RESULT_TO_SAVEACTIVITY.
     */
    public static final String RESULT_TO_SAVEACTIVITY = "Result of the classification";

    //Private variables for widgets
    private ImageView imageView;
    private TextView classText;
    private ProgressBar loading;
    private ImageButton switchCamera;
    private ImageButton capture;
    private Button saveBtn;
    private Button swtBtn;

    private String  mCurrentPhotoPath;
    private int cameraId;
    private Camera mCamera;
    private CameraPreview mPreview;
    private Camera.PictureCallback mPicture;
    private Context myContext;
    private boolean cameraFront;
    private boolean notDisplayImage;
    private ActionBar actionBar;

    /**
     * Creation of activity with association of the widgets from layout
     * @param savedInstanceState the saved state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(CAM_TAG, "onCreate()");
        setContentView(R.layout.camera_preview);

        //Setting the title of the toolbar
        Toolbar toolbar = findViewById(R.id.my_toolbar);
        toolbar.setTitle(R.string.toolbar_title_camera_activity);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        actionBar = getSupportActionBar();
        if(actionBar != null) {
            // Enable the Up button
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //ImageView and ClassText used to show the result of the classification
        imageView = findViewById(R.id.img);
        classText = findViewById(R.id.classText);

        //saveBtn and swtBtn buttons used to save or retake the classified image
        saveBtn = findViewById(R.id.save_btn);
        swtBtn = findViewById(R.id.retry_btn);

        //capture and switchCamera button used to take the picture or change the camera
        capture = findViewById(R.id.btnCam);
        switchCamera = findViewById(R.id.btnSwitch);

        //Loading bar
        loading = findViewById(R.id.loading_class);

        notDisplayImage = true;

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        myContext = this;

        //Restore of the previous instance state
        if (savedInstanceState != null)
        {
            // Restore cameraId state from the savedInstanceState
            cameraId = savedInstanceState.getInt("cameraID");

            // Restore cameraFront state from the savedInstanceState
            cameraFront = savedInstanceState.getBoolean("cameraFront");

        }

        //check to write on memory and camera permissions
        checkPermissions();
    }

    /**
     * Saving instance state of all necessary variables: in this case the camera used is saved
     * @param savedInstanceState saved state
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.d(CAM_TAG, "SavedInstanceState()");

        // Saving cameraId state in the savedInstanceState
        savedInstanceState.putInt("cameraID", cameraId);

        // Saving cameraFront state in the savedInstanceState
        savedInstanceState.putBoolean("cameraFront", cameraFront);

    }

    /**
     * Check to write on memory and camera permission and open of
     * camera preview in case of confirmation
     */
    private void checkPermissions() {

        //checking for write permission : Write Store and Camera
        String[] PERMISSIONS = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA
        };

        //Check of permission
        if (!hasPermissions(myContext, PERMISSIONS)) {
            Log.d(CAM_TAG, "Permission asked");
            //request of permissions not confirmed
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            Log.d(CAM_TAG, "Permission asked");
        } else {
            //open camera preview
            openCameraTakePicture();
        }
    }

    /**
     * Check of every request permission
     *
     * @param context     context of check permission
     * @param permissions array of string with the requested permissions
     * @return boolean that confirm all the permission
     */
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    Log.d(CAM_TAG, "Permission not grated: " + permission);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Check results requested permission
     * @param requestCode request code associated with the permission
     * @param permissions array of string with the requested permissions
     * @param grantResults result array of the requests
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions, int[] grantResults) {
        Log.d(CAM_TAG, "Permission results");
        if (requestCode == PERMISSION_ALL) {

            // If request is denied, the result array are zero.
            //Log.i(CAM_TAG, Arrays.toString(grantResults));
            if (grantResults.length > 0 && checkResult(grantResults)) {

                // permission was granted! So start the camera preview
                openCameraTakePicture();

            } else {

                // permission denied! So show a toast explaining why the app is limited
                Toast.makeText(CameraActivity.this, R.string.deniedPermission_warning, Toast.LENGTH_SHORT).show();

            }
        }
    }

    /**
     * Check result array is granted is granted in every element
     *
     * @param grantResults result array of the requests
     * @return the boolean
     */
    public static boolean checkResult(int[] grantResults) {
        for (int result : grantResults) {
            //if all requested are accepted
            if (result != PackageManager.PERMISSION_GRANTED)
                return false;

        }
        return true;
    }

    /**
     * Setting the camera preview with orientation handling and button listeners: in this part all
     * elements of the preview layout are prepared
     */
    private void openCameraTakePicture() {

        Log.d(CAM_TAG, "openCameraTakePicture()");

        //In this case camera permission is granted
        mCamera = Camera.open(cameraId);

        //set camera to continually auto-focus
        Camera.Parameters params = mCamera.getParameters();
        if (params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            mCamera.setParameters(params);
        }
        //set orientation of camera : handling of camera in portrait and landscape orinetation
        setCameraDisplayOrientation(this,cameraId,mCamera,false);

        // camera preview builded in a Linear Layout
        LinearLayout cameraPreview = findViewById(R.id.cPreview);
        mPreview = new CameraPreview(myContext, mCamera);
        cameraPreview.addView(mPreview);


        //listener associated with the layout widget: TAKE PICTURE BUTTON
        capture = findViewById(R.id.btnCam);
        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //saving image and classification
                mPicture = getPictureCallback();
                //stop preview on image classified
                mCamera.takePicture(null, null, mPicture);

                //disable button after the image is taken
                disable_buttons(capture, switchCamera);

                //Visibility of loading bar
                loading.setVisibility(View.VISIBLE);

                // Disable the Up button
                actionBar.setDisplayHomeAsUpEnabled(false);


            }
        });

        //listener associated with the layout widget: SWITCH CAMERA BUTTON
        switchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get the number of cameras
                int camerasNumber = Camera.getNumberOfCameras();
                if (camerasNumber > 1) {

                    //release the old camera instance
                    releaseCamera();

                    //switch camera, from the front and the back and vice versa
                    chooseCamera();
                }
            }
        });

    }
    /**
     * Find front camera ID
     */
    private int findFrontFacingCamera() {

        cameraId = -1;
        // Search for the front facing camera in all cameras: checking info
        //get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        //for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {

                //actual camera ID used
                cameraId = i;

                //front camera is used now
                cameraFront = true;

                break;
            }
        }
        return cameraId;

    }
    /**
     * Find back camera ID
     */
    private int findBackFacingCamera() {
        cameraId = -1;
        // Search for the back facing camera in all cameras: checking info
        //get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        //for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                //actual camera ID used
                cameraId = i;
                //back camera is used now
                cameraFront = false;
                break;

            }

        }
        return cameraId;
    }

    /**
     * Switch camera used to take picture, and so for preview
     */
    public void chooseCamera() {
        //if the camera preview is the front
        if (cameraFront) {
            cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                mCamera = Camera.open(cameraId);

                //set camera to continually auto-focus
                Camera.Parameters params = mCamera.getParameters();
                if (params.getSupportedFocusModes().contains(
                        Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                    params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                    mCamera.setParameters(params);
                }

                //set orientation of camera : handling of camera in portrait and landscape orinetation
                setCameraDisplayOrientation(this,cameraId,mCamera,false);

                //set a picture callback
                mPicture = getPictureCallback();

                //refresh the preview
                mPreview.refreshCamera(mCamera);
            }
        } else {
            cameraId = findFrontFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                mCamera = Camera.open(cameraId);

                //set camera to continually auto-focus
                Camera.Parameters params = mCamera.getParameters();
                if (params.getSupportedFocusModes().contains(
                        Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                    params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                    mCamera.setParameters(params);
                }

                //set orientation of camera : handling of camera in portrait and landscape orinetation
                setCameraDisplayOrientation(this,cameraId,mCamera,false);

                //set a picture callback
                mPicture = getPictureCallback();

                //refresh the preview
                mPreview.refreshCamera(mCamera);
            }
        }
    }

    /**
     * Stop and release camera: it is necessary before to close/change the camera
     */
    private void releaseCamera() {
        // stop preview and release camera
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;

        }
    }

    /**
     * Handle the classification result and SaveActivity result
     * @param requestCode request code associated to che classification
     * @param resultCode confirmation of classification occurred
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        try {

            //catch of classification request code
            if (requestCode == REQUEST_CLASSIFY) {
                final String result;
                //check of classification result
                if (resultCode == RESULT_OK) {
                    //Obtain the result of classification
                    result = intent.getStringExtra("result");

                    //Stop loading bar and show the result
                    loading.setVisibility(View.INVISIBLE);

                    // Enable the Up button
                    actionBar.setDisplayHomeAsUpEnabled(true);

                    //The classificationMode after the result is received
                    classificationMode();

                    //Show the result on the layout
                    classText.setText("Gettalo nel/la " + result + "!");

                    //listener associated with the layout widget: SAVE BUTTON
                    saveBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //In this case this variable is useful to indicate that the state onPause()
                            //is due to the saving of the class so the image has not to be deleted
                            //because is presented in the SaveActivity
                            notDisplayImage = true;

                            //creation of intent
                            Intent intent = new Intent(myContext, SaveActivity.class);
                            //include the path of image
                            intent.putExtra("BitmapPath", mCurrentPhotoPath);
                            intent.putExtra(RESULT_TO_SAVEACTIVITY, result);
                            startActivityForResult(intent, REQUEST_SAVING);

                        }
                    });

                    //listener associated with the layout widget: RETRY BUTTON
                    swtBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Restart activity
                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);
                        }
                    });

                } else{
                    //Handling the error of the classifcator
                    Log.d(CAM_TAG, "No result from classification");
                    result = null;
                }

            }
            else if (requestCode == REQUEST_SAVING) {
                //file deleted previously since the saving of the class is ended fine
                if(resultCode == RESULT_OK){
                    //Restart the activity
                    Intent newIntent = getIntent();
                    finish();
                    startActivity(newIntent);
                }
                //the procedure of saving was not been completed
                else {
                    //delete the image taken
                    File imgFile = new File(mCurrentPhotoPath);
                    imgFile.delete();
                    // Restart activity
                    Intent newIntent = getIntent();
                    finish();
                    startActivity(newIntent);
                }
            }
        } catch (Exception error) {
            error.printStackTrace();
        }
    }

    /**
     * onResume has to handle the start of camera preview also in case of
     * landscape orientation and switch camera: the preview is restarted every time the activity
     * pass to the state onPause()
     */
    @Override
    public void onResume() {
        super.onResume();
        Log.d(CAM_TAG, "onResume()");

        //Permission array requested to the user
        String[] PERMISSIONS = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA
        };

        //Check of guaranteed permissions and not classification ongoing
        if( notDisplayImage && hasPermissions(myContext, PERMISSIONS)) {

            //In this case the preview mode is set since the picture is not
            //already taken
            previewMode();

            //open the Camera
            mCamera = Camera.open(cameraId);

            //set camera to continually auto-focus
            Camera.Parameters params = mCamera.getParameters();
            if (params.getSupportedFocusModes().contains(
                    Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                mCamera.setParameters(params);
            }

            //set orientation of camera : handling of camera in portrait and landscape orientation
            setCameraDisplayOrientation(this,cameraId,mCamera,false);

            //set a picture callback
            mPicture = getPictureCallback();

            //start the preview
            mPreview.refreshCamera(mCamera);
        }else {
            Log.d(CAM_TAG, "onResume null");
            releaseCamera();
        }


    }

    /**
     * previewMode method prepare the layout to take the picture of classification
     */
    private void previewMode(){

        //ImageView and ClassText are make invisible: these elements show the result of classification
        imageView.setVisibility(View.INVISIBLE);
        classText.setVisibility(View.INVISIBLE);

        //Buttons saveBtn and swtBtn are set invisible and unclickable to avoid errors
        saveBtn.setVisibility(View.INVISIBLE);
        swtBtn.setVisibility(View.INVISIBLE);
        saveBtn.setClickable(false);
        swtBtn.setClickable(false);

        //Buttons capture and switchCamera are set visible and clickable to allow the user to take the picture
        capture.setVisibility(View.VISIBLE);
        switchCamera.setVisibility(View.VISIBLE);
        capture.setClickable(true);
        switchCamera.setClickable(true);

    }

    /**
     * classificationMode method prepare the layout to show the result of classification
     */
    private void classificationMode(){

        //ImageView and ClassText are make visible: these elements show the result of classification
        imageView.setBackgroundResource(R.color.colorPrimary);
        imageView.setVisibility(View.VISIBLE);

        classText.setTextColor(Color.BLACK);
        classText.setGravity(Gravity.CENTER);
        classText.setVisibility(View.VISIBLE);


        //Buttons saveBtn and swtBtn are set visible and clickable to allow the user to save or retake the picture
        saveBtn.setVisibility(View.VISIBLE);
        swtBtn.setVisibility(View.VISIBLE);

        //useful variable to indicate the preview is stopped
        notDisplayImage = false;
    }


    /**
     * onPause release camera in order to be used from other applications and delete the image taken unless
     * the user press the button save
     */
    @Override
    protected void onPause() {
        Log.d(CAM_TAG, "onPause()");
        super.onPause();

        //The image will be deleted every time the user doesn't saved it
        if(!notDisplayImage){
            File imgFile = new File(mCurrentPhotoPath);
            if(imgFile.exists()){
                imgFile.delete();
            }
            notDisplayImage = true;
        }

        //Release the camera to allow other application to use it
        releaseCamera();
    }


    /**
     * This method obtains save the taken image and start the classification
     */
    private Camera.PictureCallback getPictureCallback() {

        Camera.PictureCallback picture = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {



                //rotation of bitmap based on the orientation of camera and which camera used (back or front camera)
                int degrees = setCameraDisplayOrientation(CameraActivity.this,cameraId,mCamera,true);
                Matrix matrix = new Matrix();
                //Decide the rotation based on which camera is used
                if(!cameraFront)
                    matrix.postRotate(degrees);
                else{
                    matrix.postRotate(degrees-180);
                    matrix.preScale(1.0f, -1.0f);
                }
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

                //create a file where to save the image and save the path of the file
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File...
                }

                try (FileOutputStream out = new FileOutputStream(photoFile)) {
                    //In this case the most important criteria is the classification rapidity
                    // so the JPEG compression results the best choice
                    rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
                    out.close();

                    //start the classification
                    classify();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };

        return picture;
    }

    /**
     * Disable of button of camera layout
     */
    private void disable_buttons(ImageButton btn_cam, ImageButton btn_swt) {

        btn_cam.setClickable(false);
        btn_swt.setClickable(false);
    }

    /**
     * The classification is done through ImageClassifier class. This latter is called with an intent
     * including the image path
     */
    private void classify() {

        //creation of intent
        Intent intent = new Intent(myContext, ImageClassifier.class);
        //include the path of image
        intent.putExtra("BitmapPath", mCurrentPhotoPath);
        CameraActivity.this.startActivityForResult(intent, REQUEST_CLASSIFY);

    }

    /**
     * Creation of image file with name comprising the actual data of creation
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /**
     * Set the camera orientation based on the display orientation
     *
     * @param activity the activity
     * @param cameraId the camera id
     * @param camera   the camera
     * @param res      the res
     * @return the camera display orientation
     */
    public static int setCameraDisplayOrientation(Activity activity,
                                                  int cameraId, android.hardware.Camera camera, boolean res) {
        //Get camera info
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        //get rotation of display
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {

            //Portrait
            case Surface.ROTATION_0: degrees = 0; break;
            //Landscape
            case Surface.ROTATION_90: degrees = 90; break;
            //Portrait
            case Surface.ROTATION_180: degrees = 180; break;
            //Landscape
            case Surface.ROTATION_270: degrees = 270; break;
        }

        //decide how rotate the camera based of diplay orientation
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            //front camera
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror

        } else {
            // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        if(!res) camera.setDisplayOrientation(result);

        return result;
    }



}