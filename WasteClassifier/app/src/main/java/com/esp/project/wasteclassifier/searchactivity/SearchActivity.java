package com.esp.project.wasteclassifier.searchactivity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.esp.project.wasteclassifier.R;

import java.util.List;

import database.Waste;
import database.WasteBaseHelper;

/**
 * This class provides the implementation of the search activity and its functionalities.
 */
public class SearchActivity extends AppCompatActivity {
	
	private RecyclerView recyclerView;
	private SearchView searchView;	// declared here to restore its state
	private String strSearchView; 	// string to save the SearchView state
	private static final String SEARCH_VIEW_KEY = "search";

	/**
	 * Creates the search activity
	 * @param savedInstanceState the saved state
	 */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);

		// restore eventually the SearchView state
		if (savedInstanceState != null) {
			strSearchView = savedInstanceState.getString(SEARCH_VIEW_KEY);
		}

		// show the toolbar
		Toolbar toolbar = findViewById(R.id.my_toolbar);
		setSupportActionBar(toolbar);

		// Get a support ActionBar corresponding to this toolbar
		ActionBar actionBar = getSupportActionBar();
		if(actionBar != null) {
			// don't show the application name
			actionBar.setDisplayShowTitleEnabled(false);
			// Enable the Up button
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		
		//init the view
		recyclerView = findViewById(R.id.recycleView);
		recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
		registerForContextMenu(recyclerView);
		
		// show all the records
		showWaste(null);
	}

	/**
	 * Creates the menu in the toolbar
	 * @param menu the reference to the Menu object
	 * @return true for the menu to be displayed; false it will not be shown.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the activity_main_menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_search_menu, menu);
		
		// Associate searchable configuration with the SearchView
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		MenuItem searchMenuItem = menu.findItem(R.id.search_view_item);
		searchView = (SearchView) searchMenuItem.getActionView();

		// don't show the magnifying glass
		ImageView magImage = searchView.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
		magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setIconifiedByDefault(false);
		searchView.setMaxWidth(Integer.MAX_VALUE);

		// focus the SearchView
		if(strSearchView != null && !strSearchView.isEmpty()) {
			searchView.setQuery(strSearchView, false);
			searchView.clearFocus();
			showWaste(strSearchView);
		}

		// show the list according to the query in input
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			/**
			 * Called when the query text is changed by the user
			 * @param s the new content of the query text field
			 * @return false if the SearchView should perform the default action of showing
			 * any suggestions if available, true if the action is handled by the listener
			 */
			@Override
			public boolean onQueryTextChange(String s) {
				showWaste(s);
				return false;
			}

			/**
			 * Called when the user submits the query
			 * @param s the query text that is to be submitted
			 * @return true if the query has been handled by the listener,
			 * false to let the SearchView perform the default action.
			 */
			@Override
			public boolean onQueryTextSubmit(String s) {
				showWaste(s);
				return false;
			}
		});
		
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Save the actual query of the user
	 * @param savedInstanceState Bundle in which to place the saved state
	 */
	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		strSearchView = searchView.getQuery().toString();
		savedInstanceState.putString(SEARCH_VIEW_KEY, strSearchView);
	}
	
	/**
	 * Show the records saved in the database according to "query".
	 * If query is null, then all the records will be shown
	 * @param query the query to search
	 */
	private void showWaste(String query)
	{
		WasteBaseHelper wasteBaseHelper = new WasteBaseHelper(this);
		// show all the records
		List<Waste> wasteList;
		if(query == null || query.isEmpty()) {
			wasteList = wasteBaseHelper.findAll();
		}
		else {
			wasteList = wasteBaseHelper.find(query);
		}
		
		WasteListAdapter wasteListAdapter = new WasteListAdapter(this, wasteList);
		recyclerView.setAdapter(wasteListAdapter);
		recyclerView.setLayoutManager(new LinearLayoutManager(this));
	}

	/**
	 * Manage new intents: in particular intents coming from the voice searches of the user
	 * @param intent the reference of the intent
	 */
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);
			searchView.setQuery(query, false);
		}
	}
}
