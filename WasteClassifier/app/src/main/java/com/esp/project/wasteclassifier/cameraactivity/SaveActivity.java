package com.esp.project.wasteclassifier.cameraactivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.esp.project.wasteclassifier.R;

import java.io.File;

import database.WasteBaseHelper;

/**
 * This class provides the implementation of the save activity and its functionalities.
 */
public class SaveActivity extends AppCompatActivity {

    /**
     * The constant MAX_LENGTH.
     */
    public static final int MAX_LENGTH = 50;

    private static final String SAVE_TAG = "Saving activity--->";
    private ImageView imageView;
    private TextView panelString;
    private EditText edit;
    private Button save_name_btn;
    private Button cancel_btn;
    private File imgFile;
    private boolean savingMode;
    private static final int TIME_OUT = 2000; //Time to launch the another activity
    private static final int SAVING_OK = 1;
    private static final int changeSize = 12;
    private static final int size = 24;

    /**
     * Called when the activity is starting.
     * @param savedInstanceState the saved state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(SAVE_TAG, "Begin of SaveActivity");

        setContentView(R.layout.activity_save);

        Toolbar toolbar = findViewById(R.id.my_toolbar);
        imageView = findViewById(R.id.panel);
        panelString = findViewById(R.id.panel_textView);
        save_name_btn = findViewById(R.id.save_name_btn);
        cancel_btn = findViewById(R.id.cancel_btn);
        edit = findViewById(R.id.editText);

        toolbar.setTitle(R.string.toolbar_title_save_activity);
        setSupportActionBar(toolbar);

        //Get intent from CameraActivity
        Intent intent = getIntent();

        //Get taken image path
        String Path = intent.getStringExtra("BitmapPath");

        imgFile = new File(Path);

        Log.d(SAVE_TAG, Path);
        if(imgFile.exists()){

            Log.d(SAVE_TAG, Path);
            Bitmap bitmap = BitmapFactory.decodeFile(Path);
            ImageView myImage = findViewById(R.id.img_save);
            myImage.setImageBitmap(bitmap);
        }

        TextView classText = findViewById(R.id.save_box);
        classText.setGravity(Gravity.CENTER);
        classText.setTextColor(Color.WHITE);
        classText.setText(R.string.save_string);

        //listener associated with the layout widget: SAVE BUTTON
        savingMode = false;
        save_name_btn.setOnClickListener(new View.OnClickListener() {
            /**
             * Called when a view has been clicked.
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {

                if(savingMode){

                    String wasteName = edit.getText().toString();
                    wasteName = wasteName.substring(0, 1).toUpperCase() + wasteName.substring(1).toLowerCase();
                    String result = getIntent().getStringExtra(CameraActivity.RESULT_TO_SAVEACTIVITY);
                    if (result != null) {
                        WasteBaseHelper wasteBaseHelper = new WasteBaseHelper(SaveActivity.this);
                        wasteBaseHelper.insertContainerDate(result, System.currentTimeMillis());
                        wasteBaseHelper.insertUserWaste(wasteName, result);
                    }

                    edit.setClickable(false);
                    save_name_btn.setClickable(false);
                    cancel_btn.setClickable(false);
                    save_name_btn.setVisibility(View.INVISIBLE);
                    cancel_btn.setVisibility(View.INVISIBLE);


                    imageView.setBackgroundResource(R.color.colorPrimary);
                    imageView.setVisibility(View.VISIBLE);

                    panelString.setVisibility(View.VISIBLE);
                    panelString.setTextColor(Color.BLACK);
                    panelString.setGravity(Gravity.CENTER);
                    panelString.setText(R.string.save_panel_string);

                    //delete image taken after the saving
                    imgFile.delete();
                    // Restart camera activity
                    //creation of intent after 4 seconds
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //creation of intent
                            Intent intent = new Intent(SaveActivity.this, CameraActivity.class);
                            setResult(RESULT_OK,intent);
                            finish();
                        }
                    }, TIME_OUT);
                }
                else{
                    Toast t = Toast.makeText(SaveActivity.this, R.string.minLength_warning, Toast.LENGTH_SHORT);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                }


            }
        });

        //listener associated with the layout widget: CANCEL BUTTON
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            /**
             * Called when a view has been clicked.
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {


                edit.setClickable(false);
                save_name_btn.setClickable(false);
                cancel_btn.setClickable(false);
                save_name_btn.setVisibility(View.INVISIBLE);
                cancel_btn.setVisibility(View.INVISIBLE);

                imageView.setBackgroundResource(R.color.colorPrimary);
                imageView.setVisibility(View.VISIBLE);

                panelString.setVisibility(View.VISIBLE);
                panelString.setTextColor(Color.BLACK);
                panelString.setGravity(Gravity.CENTER);
                panelString.setText(R.string.cancel_panel_string);

                //delete image taken after the saving
                imgFile.delete();

                // Restart camera activity
                //creation of intent after 4 seconds
                //creation of intent
                Intent intent = new Intent(SaveActivity.this, CameraActivity.class);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        edit.setFilters(new InputFilter[] {new InputFilter.LengthFilter(MAX_LENGTH)});
        edit.addTextChangedListener(new TextWatcher() {
            /**
             * This method is called to notify the user if the name of the waste is empty
             * and when the he reaches the maximum number of characters allowed and set the appropriate
             * variable to indicate the name can be saved or not.
             * @param s the sequence of characters
             * @param start the starting point
             * @param before the length of the old text
             * @param count the number of characters changed
             */
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length() == 0){
                    Toast t = Toast.makeText(SaveActivity.this, R.string.minLength_warning, Toast.LENGTH_SHORT);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                    savingMode = false;
                }
                else{
                    savingMode = true;
                }

                if(s.length() == MAX_LENGTH) {
                    Toast t = Toast.makeText(SaveActivity.this, R.string.maxLength_warning, Toast.LENGTH_SHORT);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }
}