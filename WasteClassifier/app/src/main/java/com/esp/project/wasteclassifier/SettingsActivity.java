package com.esp.project.wasteclassifier;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

/**
 * This class provides the implementation of the settings activity and its functionalities.
 */
public class SettingsActivity extends AppCompatActivity {
    private SharedPreferences sharedPreferences;

    /**
     * Called when the activity is starting.
     * @param savedInstanceState the saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = findViewById(R.id.settings_toolbar);


        toolbar.setTitle("Impostazioni");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_left));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settingsContent, new SettingsFragment())
                .commit();
    }

    /**
     * This class provides the implementation of the fragments for the settings as suggested in the Google documentation.
     */
    public static class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.preferences, rootKey);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            // Figure out which preference was changed
            Preference preference = findPreference(key);
            if (null != preference) {
                // Updates the summary for the preference
                Log.e("KEY: ", key);

                if(key.equals("statsPeriod")) {
                    String value = sharedPreferences.getString(preference.getKey(), "");
                    Log.e("VALUE: ", value);
                    sharedPreferences.edit().putInt("periodValue", Integer.parseInt(value)).apply();
                }

                if(key.equals("languageType")) {
                    String value = sharedPreferences.getString(preference.getKey(), "");
                    Log.e("VALUE: ", value);
                    sharedPreferences.edit().putInt("language", Integer.parseInt(value)).apply();
                }

                if(key.equals("sendResults")) {
                    boolean value = sharedPreferences.getBoolean(preference.getKey(), false);
                    Log.e("VALUE: ", "" + value);
                    sharedPreferences.edit().putBoolean("sendResults", value).apply();
                }

            }
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
            super.onPause();
        }
    }
}