package database;

import java.io.Serializable;

/**
 * This class represents a waste: it has a name and a container
 */
public class Waste implements Serializable
{
	private String name;
	private String container;

    /**
     * Returns an empty waste
     */
    public Waste()
	{
		name = null;
		container = null;
	}

    /**
     * Return a waste
     *
     * @param name      the name of the waste
     * @param container the container in which the waste must be thrown
     */
    public Waste(String name, String container)
	{
		this.name = name;
		this.container = container;
	}

    /**
     * Gets name.
     *
     * @return the name of the waste
     */
    public String getName() {
		return name;
	}

    /**
     * Sets name.
     *
     * @param name set the name of the waste
     */
    public void setName(String name) {
		this.name = name;
	}

    /**
     * Gets container.
     *
     * @return the container of the waste
     */
    public String getContainer() {
		return container;
	}

    /**
     * Sets container.
     *
     * @param container set the container of the waste
     */
    public void setContainer(String container) {
		this.container = container;
	}
	
}
