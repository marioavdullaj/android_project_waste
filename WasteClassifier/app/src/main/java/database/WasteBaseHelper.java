package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import database.WasteDbSchema.RegisterContainer;
import database.WasteDbSchema.UserWaste;
import database.WasteDbSchema.WasteTable;

/**
 * A helper class to manage the waste database creation and version management
 */
public class WasteBaseHelper extends SQLiteOpenHelper
{
    private static final int VERSION = 1;

    /**
     * Creates a new WasteBaseHelper with the given context
     *
     * @param context the context of the activity
     */
    public WasteBaseHelper(Context context)
    {
        super(context, WasteDbSchema.DATABASE_NAME, null, VERSION);
    }

    /**
     * This method is called to create the tables
     * and to fill the database
     * @param db the reference of the database
     */
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String query = "CREATE VIRTUAL TABLE IF NOT EXISTS " + WasteTable.NAME + " USING FTS4 (" +
                WasteTable.Cols.NAME + ", " +
                WasteTable.Cols.CONTAINER + ") ";
        db.execSQL(query);

        query = "CREATE VIRTUAL TABLE IF NOT EXISTS " + RegisterContainer.NAME + " USING FTS4 (" +
                RegisterContainer.Cols.NAME + ", " +
                RegisterContainer.Cols.TIME + ") ";
        db.execSQL(query);

        query = "CREATE VIRTUAL TABLE IF NOT EXISTS " + UserWaste.NAME + " USING FTS4 (" +
                UserWaste.Cols.NAME + ", " +
                UserWaste.Cols.CONTAINER + ") ";
        db.execSQL(query);

        // fill the database
        // first: parse the HTML file and fill the Set and the Map
        Map<String, String> tMap = new TreeMap<>();

        parseHTML(tMap);

        // insert the container in the table Waste
        insertWasteContainer(db, tMap);
    }

    /**
     * Upgrade the database
     * @param db the database
     * @param oldVersion the number of the old version of the database
     * @param newVersion the number of the new version of the database
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        String query = "DROP TABLE IF EXISTS " + WasteTable.NAME;
        db.execSQL(query);

        query = "DROP TABLE IF EXISTS " + RegisterContainer.NAME;
        db.execSQL(query);

        query = "DROP TABLE IF EXISTS " + UserWaste.NAME;
        db.execSQL(query);

        onCreate(db);
    }

    /**
     * Queries the database for the waste searching using the prefix
     *
     * @param prefix the prefix of the waste to search
     * @return a list of waste
     */
    public List<Waste> find(String prefix)
    {
        List<Waste> wasteList = null;

        String query = "SELECT * FROM " + WasteTable.NAME;
        if(prefix != null)
            query += " WHERE " + WasteTable.NAME + " MATCH '" + prefix + "*'";

        try {
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery(query, null);
            if (cursor.moveToFirst())
            {
                wasteList = new ArrayList<>();
                do {
                    Waste waste = new Waste(cursor.getString(0), cursor.getString(1));
                    wasteList.add(waste);
                }
                while(cursor.moveToNext());
            }
            cursor.close();
        }
        catch (Exception e)
        {
            wasteList = null;
        }

        return wasteList;
    }

    /**
     * Queries the database for all the waste inside
     *
     * @return a cursor containing all the wastes
     */
    public List<Waste> findAll()
    {
        return find(null);
    }

    /**
     * Queries the database for all the containers saved in the last milliseconds
     *
     * @param milliseconds the number of milliseconds to specify to retrieve items entered in the last "milliseconds" from now
     * @return a map containing the containers as key and their count as value
     */
    public Map<String, Long> getLastContainers(long milliseconds)
    {
        Map<String, Long> containerMap = new HashMap<>();

        String query = "SELECT * FROM " + RegisterContainer.NAME;
        try {
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery(query, null);
            long currentDate = System.currentTimeMillis();
            if (cursor.moveToFirst())
            {
                do {
                    String container = cursor.getString(0);
                    long containerDate = Long.parseLong(cursor.getString(1));
                    if(currentDate - containerDate <= milliseconds || milliseconds == 0) { // milliseconds = 0: no time filter
                        if(!containerMap.containsKey(container))
                            containerMap.put(container, 0L);

                        long count = containerMap.get(container);
                        containerMap.put(container, count+1);
                    }
                }
                while(cursor.moveToNext());
            }
            cursor.close();
        }
        catch (Exception e)
        {
            containerMap = new HashMap<>();
        }

        return containerMap;
    }

    /**
     * Queries the database for all the wastes saved by the user in a specific container
     *
     * @param containerName the name of the container whence retrieve all the wastes saved by the user
     * @return a list of waste
     */
    public List<String> findUserWaste(String containerName)
    {
        List<String> wasteList = new ArrayList<>();

        String query = "SELECT " + UserWaste.Cols.NAME
                + " FROM " + UserWaste.NAME
                + " WHERE " + UserWaste.Cols.CONTAINER + " MATCH '" + containerName + "'";

        try {
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery(query, null);
            if (cursor.moveToFirst())
            {
                do {
                    wasteList.add(cursor.getString(0));
                }
                while(cursor.moveToNext());
            }
            cursor.close();
        }
        catch (Exception e)
        {
            wasteList = new ArrayList<>();
        }

        // Sort the list in alphabetic order
        Collections.sort(wasteList);

        return wasteList;
    }

    /**
     * Insert the waste that the user wants to save with its container if it is not present
     *
     * @param wasteName     the waste the user wants to save
     * @param containerName the container
     */
    public void insertUserWaste(String wasteName, String containerName)
    {
        // check if the record is already present in the database
        String query = "SELECT " + UserWaste.Cols.CONTAINER
                + " FROM " + UserWaste.NAME
                + " WHERE " + UserWaste.Cols.NAME
                + " MATCH '" + wasteName + " "
                + UserWaste.Cols.CONTAINER + ":" + containerName + "'";
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        // insert the record if it is not present
        if(!cursor.moveToFirst()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(UserWaste.Cols.NAME, wasteName);
            contentValues.put(UserWaste.Cols.CONTAINER, containerName);
            sqLiteDatabase.insert(UserWaste.NAME, null, contentValues);
        }
        cursor.close();
    }

    /**
     * Delete the waste with its container that the user saved
     *
     * @param wasteName     the waste the user wants to delete
     * @param containerName the container of the waste to delete
     */
    public void deleteUserWaste(String wasteName, String containerName)
    {
        // check if the record is already present in the database
        String query = "DELETE FROM " + UserWaste.NAME
                + " WHERE " + UserWaste.Cols.NAME
                + " MATCH '" + wasteName + " "
                + UserWaste.Cols.CONTAINER + ":" + containerName + "'";

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        sqLiteDatabase.execSQL(query);
    }

    /**
     * Delete the all the wastes saved by the user in a specific container
     *
     * @param containerName the container of which the user wants to delete the wastes
     */
    public void deleteAllUserWastes(String containerName)
    {
        // check if the record is already present in the database
        String query = "DELETE FROM " + UserWaste.NAME
                + " WHERE " + UserWaste.Cols.CONTAINER
                + " MATCH '" + containerName + "'";

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        sqLiteDatabase.execSQL(query);
    }

    /**
     * Update the waste with its container that the user saved
     *
     * @param oldWasteName  the waste the user wants to update
     * @param newWasteName  the new name of the waste to update
     * @param containerName the container of the waste to update
     */
    public void updateUserWaste(String oldWasteName, String newWasteName, String containerName)
    {
        //UPDATE pages SET title = 'Download SQLite' WHERE rowid = 54;
        // check if the record is already present in the database
        String query = "UPDATE " + UserWaste.NAME
                + " SET " + UserWaste.Cols.NAME + " = '" + newWasteName + "'"
                + " WHERE " + UserWaste.Cols.NAME
                + " MATCH '" + oldWasteName + " "
                + UserWaste.Cols.CONTAINER + ":" + containerName + "'";

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        sqLiteDatabase.execSQL(query);
    }

    /**
     * Insert the container in the database with the timestamp
     *
     * @param container    the container to add
     * @param milliseconds the timestamp of the container
     */
    public void insertContainerDate(String container, long milliseconds)
    {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(RegisterContainer.Cols.NAME, container);
        contentValues.put(RegisterContainer.Cols.TIME, milliseconds);
        sqLiteDatabase.insert(RegisterContainer.NAME, null, contentValues);
    }

    /**
     * This method parse the res/xml/waste.html file, which contains the information
     * about the waste and the container, using an external library for the parsing (Jsoup).
     * @param tMap a map that will contain pairs where the key is a waste and the value is the container
     */
    private void parseHTML(Map<String, String> tMap)
    {
        final String PATH = "assets/waste.html";

        final InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(PATH);

        StringBuilder stringBuilder = new StringBuilder();
        Scanner sc = new Scanner(inputStream);
        while(sc.hasNextLine())
            stringBuilder.append(sc.nextLine());


        Document doc = Jsoup.parse(stringBuilder.toString());

        System.out.println(doc.title());
        Elements newsHeadlines = doc.select("td[class]");


        String trash = null;
        String container;
        for(Element headline : newsHeadlines)
        {
            String field = headline.attr("class");

            if(field.equals("views-field views-field-title"))
            {
                trash = headline.text();
            }
            else
            {
                container = headline.text();
                if(trash != null)
                    tMap.put(trash, container);
            }
        }

    }

    /**
     * This method insert all the pairs (waste, container) of tMap in the database db
     * @param db the reference to the database for inserting
     * @param tMap the map containing the pairs to insert
     */
    private static void insertWasteContainer(SQLiteDatabase db, Map<String, String> tMap) {

        ContentValues contentValues = new ContentValues();

        for (Map.Entry<String, String> entry : tMap.entrySet()) {
            contentValues.put(WasteTable.Cols.NAME, entry.getKey());
            contentValues.put(WasteTable.Cols.CONTAINER, entry.getValue());
            db.insert(WasteTable.NAME, null, contentValues);
        }
    }
}
