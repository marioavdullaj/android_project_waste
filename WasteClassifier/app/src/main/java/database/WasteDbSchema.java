package database;

/**
 * This class defines the schema of the database
 */
abstract class WasteDbSchema
{
    /**
     * The name of the database
     */
    static final String DATABASE_NAME = "wasteDatabase";

    /**
     * This inner class represents the waste table of the database
     */
    static final class WasteTable
	{
        /**
         * The name of the table
         */
        static final String NAME = "waste";

        /**
         * This inner class represents the columns of the waste table
         */
        static final class Cols
		{
            /**
             * The name of a column
             */
            static final String NAME = "waste_name";
            /**
             * The name of a column
             */
            static final String CONTAINER = "container_name";
		}
	}

    /**
     * This inner class represents the register container table of the database
     */
    static final class RegisterContainer
	{
        /**
         * The name of the table
         */
        static final String NAME = "register_container";

        /**
         * This inner class represents the columns of the register container table
         */
        static final class Cols
		{
            /**
             * The name of a column
             */
            static final String NAME = "container_name";
            /**
             * The name of a column
             */
            static final String TIME = "timestamp";
		}
	}

    /**
     * This inner class represents the register container table of the database
     */
    static final class UserWaste
	{
        /**
         * The name of the table
         */
        static final String NAME = "user_saved_waste";

        /**
         * This inner class represents the columns of the register container table
         */
        static final class Cols
		{
            /**
             * The name of a column
             */
            static final String NAME = "waste_name";
            /**
             * The name of a column
             */
            static final String CONTAINER = "container_name";
		}
	}
}
